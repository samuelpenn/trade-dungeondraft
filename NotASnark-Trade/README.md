# Traveller Asset Pack for Dungeondraft

Author: Samuel Penn (sam@notasnark.net)

License: CAL-NR

Can be used for any purpose with no restrictions.

This is a set of SciFi assets for use with Dungeondraft.
They are designed for use with the Traveller RPG, but should be useful for
any SciFi maps, especially starship floor plans.

For the source files for these assets, see:
https://gitlab.com/samuelpenn/traveller-dungeondraft

