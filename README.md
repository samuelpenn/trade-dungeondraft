# Trade Dungeondraft

Author: Samuel Penn (sam@notasnark.net)
License: CC-BY-SA

These map assets are for use with Dungeondraft, and provide containers,
cargo, logos, signs and other items for use in trade.

They can be combined with the Traveller and Starport asset packs to
provide extra assets that can be found in cargo bays or around starports.

They are specifically designed for use with Traveller, but should be useful
for any similar style of RPG.

How to define all of these is described here:
https://github.com/Megasploot/Dungeondraft/wiki/Custom-Assets-Guide


## Notes

### Standard Shipping Containers

https://wiki.travellerrpg.com/Goods/Container



## Building

Running `install.sh` will convert the GIMP XCF files and SVG files into
PNGs for use in Dungeoncraft. It will also use `dungeondraft-pack` to
build the asset pack.

If you don't have `dungeondraft-pack`, then you can build the asset pack
manually from within the Dungeondraft GUI.

For the packager, see here:
https://github.com/Ryex/Dungeondraft-GoPackager


### Dependencies

#### Fonts

Vilani Font - http://www.trisen.com/sol/default.asp?topic=10&page=123

Black Ops One - https://fonts.google.com/specimen/Black+Ops+One
